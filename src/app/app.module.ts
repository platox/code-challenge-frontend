import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { NgxsModule } from '@ngxs/store';
import { DashboardState } from './dashboard/state/dashboard.state';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    NgxsModule.forRoot([DashboardState])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
