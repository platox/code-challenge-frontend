import { Injectable } from "@angular/core";
import { Action, Selector, State, StateContext } from "@ngxs/store";
import { DummyAction } from "./dashboard.actions";

export interface DashboardStateModel {
  test: string
}

@Injectable()
@State<DashboardStateModel>({
  name: 'dashboard',
  defaults: {
    test: 'Test Me'
  }
})
export class DashboardState {
  constructor(
  ) {}

  @Selector()
  static dummySelector(state: DashboardStateModel): string {
    return state.test;
  }

  @Action(DummyAction)
  getNGSInterpretationsData(ctx: StateContext<DashboardStateModel>): void {}
}
