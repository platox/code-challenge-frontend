import { Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { DashboardState } from './state/dashboard.state';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  @Select(DashboardState.dummySelector) test?: BehaviorSubject<string>;
}
